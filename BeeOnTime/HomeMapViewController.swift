//
//  ViewController.swift
//  BeeOnTime
//
//  Created by Rares Matei on 01/03/2015.
//  Copyright (c) 2015 Bee Consultants. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        testUplooad()
        test()
        
    }
    
    func testUplooad() {
        
        let itemToInsert = ["Latitude_Start": 57.144479, "Longitude_Start": -2.114453, "Latitude_Destination": 57.148321,"Longitude_Destination":-2.105870]
        
        let delegate = UIApplication.sharedApplication().delegate as AppDelegate
        let client = delegate.client!
        let itemTable = client.tableWithName("User")
        
        itemTable!.insert(itemToInsert) {
            (item, error) in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if error != nil {
                println("Error: " + error.description)
            } else {
                println(item["Id"])
            }
        }
    }
    
    func deleteDB() {
        let url = NSURL(string: "http://www.stackoverflow.com")
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            println(NSString(data: data, encoding: NSUTF8StringEncoding))
        }
        
        task.resume()
    }
    
    func test() {
        let delegate = UIApplication.sharedApplication().delegate as AppDelegate
        let client = delegate.client!
        let itemTable = client.tableWithName("User")
        itemTable!.readWithCompletion() {
            result, totalCount, error  in
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if error != nil {
                println("Error: " + error.description)
                return
            }
            println("Information: retrieved %d records", result.count)
        }

    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if let location = locations.first as? CLLocation {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            locationManager.stopUpdatingLocation()
        }
    }


}

